# The MIT License (MIT)

# Copyright (c) 2020 Weizmann Institute of Science
# Copyright (c) 2020 Elad Noor

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


"""A script for finding all possible pathways from A to B."""
import json
import logging
import re
from pathlib import Path
from typing import Dict, Iterable, List, Set

import cobra
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from equilibrator_api import Q_, ComponentContribution
from equilibrator_api.model import Bounds
from equilibrator_pathway import EnzymeCostModel, ThermodynamicModel
from optlang import Constraint, Model, Objective, Variable, symbolics
from sbtab import SBtab
from tqdm import tqdm

import kinetics
from solution import Solution
from util import calculate_standard_dgs, cobra_met_to_equilibrator


class PathFinder(object):
    """Finds pathways using the Universal stoichiometric matrix."""

    DEFAULT_P_H = Q_(7.5)
    DEFAULT_IONIC_STRENGTH = Q_("0.3 M")
    DEFAULT_P_MG = Q_(3)
    DEFAULT_SUM_OF_FLUXES_UB = 100.0
    DEFAULT_NUM_REACTIONS_UB = 50
    DEFAULT_MDF_LB = Q_(0.0, "kJ/mol")
    DEFAULT_SEARCH_RADIUS = 3
    DEFAULT_MAX_ITERATIONS = 10
    DEFAULT_FLUX_LB = -1000.0
    DEFAULT_FLUX_UB = 1000.0

    def __init__(
        self,
        cobra_model: cobra.Model,
        bounds: Bounds,
        comp_contrib: ComponentContribution = None,
    ):
        """Create a Path for designing pathways based on goal stoichiometry.

        :param cobra_model: a COBRA model, from which to take the list of
        reactions.
        """
        self.cobra_model = cobra_model.copy()
        self.comp_contrib = comp_contrib
        self.result_dir = "."

        self.bounds = bounds or Bounds.get_default_bounds(self.comp_contrib)

        self.cofactor_set: Set[cobra.Metabolite] = set()
        self.lp_model = None
        self.__sum_of_fluxes_ub = PathFinder.DEFAULT_SUM_OF_FLUXES_UB
        self.__num_reactions_ub = PathFinder.DEFAULT_NUM_REACTIONS_UB
        self.__mdf_lb = PathFinder.DEFAULT_MDF_LB
        self.__search_radius = PathFinder.DEFAULT_SEARCH_RADIUS
        self.__max_iterations = PathFinder.DEFAULT_MAX_ITERATIONS
        self._objective_coefficients: Dict[str, float] = dict()
        self._currency_metabolites = set()

        for rxn in self.cobra_model.reactions:
            self.set_objective_coefficient(rxn, 1.0)

        if self.comp_contrib is not None:
            self.standard_dg = calculate_standard_dgs(
                self.cobra_model, self.comp_contrib
            )

    @property
    def result_dir(self) -> Path:
        return self.__result_dir

    @property
    def result_pathway_path(self) -> Path:
        return self.__result_dir / "pathways.tsv"

    @property
    def result_pathway_dir(self) -> Path:
        return self.__result_dir / "pathway"
    @property
    def result_gdot_dir(self) -> Path:
        return self.__result_dir / "gdot"
    @property
    def result_ecm_dir(self) -> Path:
        return self.__result_dir / "ecm"
    @property
    def result_enkie_dir(self) -> Path:
        return self.__result_dir / "enkie"
    @result_dir.setter
    def result_dir(self, result_dir: str | Path) -> None:
        self.__result_dir = Path(result_dir)
        self.__result_dir.mkdir(parents=True, exist_ok=True)

    def set_objective_coefficient(
        self, rxn: str | cobra.Reaction, coefficient: float
    ) -> None:
        """Set the objective coefficient of a reaction.

        :param rxn: cobra Reaction or Reaction ID
        :param coefficient: the objective coefficient
        """
        assert coefficient >= 0, "Objective coefficients must be positive or 0"
        if type(rxn) == str:
            self._objective_coefficients[rxn] = coefficient
        else:
            self._objective_coefficients[rxn.id] = coefficient

    def get_objective_coefficient(self, rxn: str | cobra.Reaction) -> float:
        """Get the objective coefficient of a reaction.

        :param rxn: cobra Reaction or Reaction ID
        :return: the objective coefficient
        """
        if type(rxn) == str:
            return self._objective_coefficients.get(rxn, 0.0)
        else:
            return self._objective_coefficients.get(rxn.id, 0.0)

    def add_currency_metabolites(
        self, currency_metabolites: Iterable[str | cobra.Metabolite]
    ) -> None:
        """Add metabolites to the set of currency metabolites

        :param currency_metabolites: iterable of metabolites to add
        """
        for met in currency_metabolites:
            if type(met) == str:
                self._currency_metabolites.add(met)
            else:
                self._currency_metabolites.add(met.id)

    @property
    def metabolites(self) -> Iterable[cobra.Metabolite]:
        """Return the list of COBRA metabolites in the model."""
        return self.cobra_model.metabolites

    @property
    def reactions(self) -> Iterable[cobra.Reaction]:
        """Return the list of COBRA reactions in the model."""

        # First check for duplicate reactions, i.e. reactions that are
        # identical in terms of stoichiometry
        rxn_hashes = []
        for rxn in self.cobra_model.reactions:
            rxn_hashes.append((rxn, str(rxn.metabolites)))
        df = pd.DataFrame(data=rxn_hashes, columns=["reaction", "hash"])
        unique_df = df.drop_duplicates(subset="hash", keep="first")

        return unique_df.reaction.values

    def get_currency_metabolites(self) -> Set[cobra.Metabolite | str]:
        """Get the set of currency metabolites (used to define type II
        cycles)."""
        return self.__currency_metabolites

    def set_currency_metabolites(self, x: Set[cobra.Metabolite | str]):
        """Set the set of currency metabolites (used to define type II
        cycles)."""
        self.__currency_metabolites = x

    @property
    def stoichiometry_df(self) -> pd.DataFrame:
        """Convert the stoichiometric data to a DataFrame.

        :return: a DataFrame containing the sparse stoichiometric matrix.
        """
        stoichiometric_data = []
        for rxn in self.reactions:
            for met in rxn.metabolites:
                stoichiometric_data.append((rxn, met, met.id, rxn.get_coefficient(met)))
        return pd.DataFrame(
            stoichiometric_data,
            columns=["reaction", "metabolite", "metabolite_id", "coefficient"],
        )

    def add_reaction(
        self,
        id: str,
        name: str,
        metabolites_to_add: Dict[cobra.Metabolite | str, float],
        lower_bound: float = DEFAULT_FLUX_LB,
        upper_bound: float = DEFAULT_FLUX_UB,
        objective_coefficient: float = 1.0,
    ):
        """Insert (or override) a reaction to the COBRA model."""
        logging.debug(f"Adding reaction: {id}")

        rxn = cobra.Reaction(
            id=id, name=name, lower_bound=lower_bound, upper_bound=upper_bound
        )
        self.cobra_model.add_reactions([rxn])
        rxn.add_metabolites(metabolites_to_add)
        self.set_objective_coefficient(rxn, objective_coefficient)
        return rxn

    def set_pathway_objective(
        self, metabolites_to_add: Dict[cobra.Metabolite | str, float]
    ) -> None:
        """Add a dictionary of the overall reaction required for this path
        search problem."""
        logging.debug("Setting pathway objective")
        self.add_reaction(
            id="__path_objective__",
            name="objective",
            metabolites_to_add=metabolites_to_add,
            lower_bound=-1 - 1e-5,
            upper_bound=-1 + 1e-5,
            objective_coefficient=0.0,
        )

    @property
    def mdf_lb(self) -> Q_:
        """Get the lower bound on the MDF."""
        return self.__mdf_lb

    @mdf_lb.setter
    def mdf_lb(self, lb: Q_) -> None:
        """Set the lower bound for the MDF. The default is 0."""
        assert lb.check(
            "[length] ** 2 * [mass] / [substance] / [time] ** 2"
        ), "MDF lower bound must be in Gibbs energy units (e.g. kJ/mol)"
        self.__mdf_lb = lb

    @property
    def sum_of_fluxes_ub(self) -> float:
        """Get the upper bound on the sum of fluxes."""
        return self.__sum_of_fluxes_ub

    @sum_of_fluxes_ub.setter
    def sum_of_fluxes_ub(self, ub: float) -> None:
        """Set the upper bound for the sum of fluxes. The default is 100."""
        self.__sum_of_fluxes_ub = ub

    @property
    def num_reactions_ub(self) -> float:
        """Get the upper bound on the number of active reactions."""
        return self.__num_reactions_ub

    @num_reactions_ub.setter
    def num_reactions_ub(self, ub: float) -> None:
        """Set the upper bound for the number of active reactions. The default is 50."""
        self.__num_reactions_ub = ub

    @property
    def search_radius(self) -> float:
        """Get the search radius."""
        return self.__search_radius

    @search_radius.setter
    def search_radius(self, radius: float) -> None:
        """Set the search radius."""
        self.__search_radius = radius

    @property
    def max_iterations(self) -> int:
        """Get the maximum number of iterations for the search."""
        return self.__max_iterations

    @max_iterations.setter
    def max_iterations(self, max_iterations: int) -> None:
        """Set the maximum number of iterations for the search."""
        self.__max_iterations = max_iterations

    def _create_min_flux_lp(self) -> None:
        """This is only for debugging purposes."""
        logging.debug("Gathering reactions from COBRA model")

        irreversible_reaction = []
        for rxn in self.reactions:
            obj_coeff = self.get_objective_coefficient(rxn)
            # Split this reaction to forward and backward
            if rxn.upper_bound > 0:
                irreversible_reaction.append(
                    (
                        rxn,
                        rxn.id + "_F",
                        1,
                        max(rxn.lower_bound, 0),
                        rxn.upper_bound,
                        obj_coeff,
                    )
                )
            if rxn.lower_bound < 0:
                irreversible_reaction.append(
                    (
                        rxn,
                        rxn.id + "_R",
                        -1,
                        max(-rxn.upper_bound, 0),
                        -rxn.lower_bound,
                        obj_coeff,
                    )
                )

        df = pd.DataFrame(
            data=irreversible_reaction,
            columns=[
                "reaction",
                "reaction_id",
                "direction",
                "lower_bound",
                "upper_bound",
                "objective_coefficient",
            ],
        )

        logging.debug(
            f"Creating flux variables and indicators, for the "
            f"{df.shape[0]} one-way reactions"
        )

        df["flux"] = [
            Variable(
                f"R@flux@{row.reaction_id}",
                lb=row.lower_bound,
                ub=row.upper_bound,
                type="continuous",
            )
            for row in df.itertuples()
        ]

        self.lp_model = Model(name="path-designer")
        logging.debug(
            f"Creating path-finding LP using solver: "
            f"{self.lp_model.interface.__name__}"
        )
        self.lp_model.configuration.verbosity = 0
        self.lp_model.update()

        # only count the irreversible fluxes
        logging.debug("Adding mass balance constraints")

        # add stoichiometry information to each irreversible reaction
        stoich_df = df.merge(self.stoichiometry_df, on="reaction")

        # adjust the sign of coefficients to the new "irreversible" directions
        stoich_df.coefficient = stoich_df.coefficient.multiply(stoich_df.direction)

        # add the flux variables to the DataFrame
        stoich_df["flux_times_coeff"] = stoich_df.flux.multiply(stoich_df.coefficient)

        mass_balance_constraints = []
        for met_id, group_df in stoich_df.groupby("metabolite_id"):
            if group_df.shape[0] == 1:
                expr = group_df.flux_times_coeff.iat[0]
            else:
                expr = symbolics.add(group_df.flux_times_coeff)
            mass_balance_constraints.append(
                Constraint(expr, lb=0.0, ub=0.0, name=f"M@mass_balance@{met_id}")
            )

        self.lp_model.add(mass_balance_constraints)
        self.lp_model.update()

        sum_of_fluxes = symbolics.add(df.flux)

        logging.debug("Adding minimum sum of all indicators objective")
        self.lp_model.objective = Objective(
            sum_of_fluxes, direction="min", name="sum_of_fluxes"
        )
        self.lp_model.update()

    def create_min_step_lp(self) -> None:
        """Create the LP for finding the path with min number of reactions."""
        logging.debug("Gathering reactions from COBRA model")

        irreversible_reaction = []
        for rxn in self.reactions:
            obj_coeff = self.get_objective_coefficient(rxn)
            # Split this reaction to forward and backward
            if rxn.upper_bound > 0:
                irreversible_reaction.append(
                    (
                        rxn,
                        rxn.id + "_F",
                        1,
                        max(rxn.lower_bound, 0),
                        rxn.upper_bound,
                        obj_coeff,
                    )
                )
            if rxn.lower_bound < 0:
                irreversible_reaction.append(
                    (
                        rxn,
                        rxn.id + "_R",
                        -1,
                        max(-rxn.upper_bound, 0),
                        -rxn.lower_bound,
                        obj_coeff,
                    )
                )

        df = pd.DataFrame(
            data=irreversible_reaction,
            columns=[
                "reaction",
                "reaction_id",
                "direction",
                "lower_bound",
                "upper_bound",
                "objective_coefficient",
            ],
        )

        logging.debug(
            f"Creating flux variables and indicators, for the "
            f"{df.shape[0]} one-way reactions"
        )

        df["flux"] = [
            Variable(
                f"R@flux@{row.reaction_id}",
                lb=row.lower_bound,
                ub=row.upper_bound,
                type="continuous",
            )
            for row in df.itertuples()
        ]

        df["gamma"] = [
            Variable(f"R@gamma@{row.reaction_id}", type="binary")
            if row.objective_coefficient > 0
            else None
            for row in df.itertuples()
        ]
        df_obj = df[df.objective_coefficient > 0].copy()

        logging.debug("Creating indicator/flux constraints")
        # Make each gamma into a flux indicator
        # (i.e. so that if v_i > 0 then gamma_i must be equal to 1).
        # We use the following constraint:
        #          - M - 1 <= v_i - M * gamma_i <= 0
        df_obj["indicator_constraint"] = [
            Constraint(
                row.flux - row.upper_bound * row.gamma,
                lb=(-row.upper_bound - 1.0),
                ub=0.0,
                name=f"R@constr_gamma@{row.reaction_id}",
            )
            for row in df_obj.itertuples()
        ]

        self.lp_model = Model(name="path-designer")
        logging.debug(
            f"Creating path-finding LP using solver: "
            f"{self.lp_model.interface.__name__}"
        )
        self.lp_model.configuration.verbosity = 0
        self.lp_model.add(df_obj.indicator_constraint)
        self.lp_model.update()

        # only count the irreversible fluxes
        logging.debug(
            f"Set 'sum of fluxes' upper bound: " f"{self.sum_of_fluxes_ub}"
        )
        sum_of_fluxes = symbolics.add(df[df.objective_coefficient > 0].flux)
        self.lp_model.add(
            Constraint(
                sum_of_fluxes, lb=0, ub=self.sum_of_fluxes_ub, name="sum_of_fluxes"
            )
        )

        logging.debug(
            f"Set 'number of active reactions' upper bound: " f"{self.num_reactions_ub}"
        )
        weighted_sum_of_gammas = symbolics.add(
            df_obj.gamma.multiply(df_obj.objective_coefficient)
        )
        self.lp_model.add(
            Constraint(
                weighted_sum_of_gammas, lb=0, ub=self.num_reactions_ub, name="num_reactions"
            )
        )

        logging.debug("Adding mass balance constraints")

        # add stoichiometry information to each irreversible reaction
        stoich_df = df.merge(self.stoichiometry_df, on="reaction")

        # adjust the sign of coefficients to the new "irreversible" directions
        stoich_df.coefficient = stoich_df.coefficient.multiply(stoich_df.direction)

        # add the flux variables to the DataFrame
        stoich_df["flux_times_coeff"] = stoich_df.flux.multiply(stoich_df.coefficient)

        mass_balance_constraints = []
        for met_id, group_df in stoich_df.groupby("metabolite_id"):
            if group_df.shape[0] == 1:
                expr = group_df.flux_times_coeff.iat[0]
            else:
                expr = symbolics.add(group_df.flux_times_coeff)
            mass_balance_constraints.append(
                Constraint(expr, lb=0.0, ub=0.0, name=f"M@mass_balance@{met_id}")
            )

        self.lp_model.add(mass_balance_constraints)
        self.lp_model.update()

        if self._currency_metabolites:
            self._add_loopless_constraints(stoich_df)

        if self.comp_contrib is not None:
            logging.debug("Adding OptMDFpathway objective")
            margin = self._add_optmdf_constraints(stoich_df)
            # the new objective is the max MDF, but we also minimize the sum of
            # fluxes as a secondary objective
            self.lp_model.objective = Objective(
                margin - weighted_sum_of_gammas, direction="max", name="mdf"
            )
            self.lp_model.update()
        else:
            logging.debug("Adding minimum sum of all indicators objective")
            self.lp_model.objective = Objective(
                weighted_sum_of_gammas, direction="min", name="weighted_sum_of_gammas"
            )
            self.lp_model.update()

    def _add_loopless_constraints(self, stoich_df: pd.DataFrame) -> None:
        """Constraints that remove type II futile cycles.

        In other words, loops that only cycle currency metabolites. To
        achieve this, we create a set of metabolite potential variables and
        add them to the big DataFrame.

        :param stoich_df: a DataFrame containing all variables and the
        stoichiometry
        """
        non_currency_met_ids = set(stoich_df.metabolite_id).difference(
            self._currency_metabolites
        )

        metabolite_potentials = pd.Series(
            {
                met_id: Variable(f"M@loopless@{met_id}", type="continuous")
                for met_id in non_currency_met_ids
            },
            name="M@loopless",
        )
        df_without_currency = stoich_df.join(
            metabolite_potentials, on="metabolite_id", how="inner"
        )

        # Then we calculate the reaction potentials (by multiplying the
        # the metabolite potential vector by S).
        df_without_currency[
            "G_loopless_times_coeff"
        ] = df_without_currency.G_loopless.multiply(df_without_currency.coefficient)

        loopless_constraints = []
        large_energy_value = 1000.0
        driving_force_margin = 1e-1
        logging.debug("Adding loop-less rules to remove type II cycles")
        for rxn_id, group_df in df_without_currency.groupby("reaction_id"):
            if rxn_id[0:2] != "__":
                gamma = group_df.gamma.iat[0]
                G_r = symbolics.add(group_df.G_loopless_times_coeff.values)
                # Add the constraints on G_i, i.e. G_i < 0 if gamma_i = 1:
                #          -M <= G_r + (M + epsilon) * gamma_i <= M
                loopless_constraints.append(
                    Constraint(
                        G_r + (large_energy_value + driving_force_margin) * gamma,
                        lb=-large_energy_value,
                        ub=large_energy_value,
                        name=f"R@loopless@{rxn_id}",
                    )
                )
        self.lp_model.add(loopless_constraints)
        self.lp_model.update()

    def _add_optmdf_constraints(self, stoich_df: pd.DataFrame) -> Variable:
        """Add the thermodynamic constraints (TFA)

        :param stoich_df: a DataFrame containing all variables and the
        stoichiometry
        """

        # create the ln concentration variables, and give them the proper bounds

        logging.debug("Defining log-concentration variables")

        compounds = []
        for met_id in stoich_df.metabolite_id:
            met = self.cobra_model.metabolites.get_by_id(met_id)
            try:
                _, compound = cobra_met_to_equilibrator(met.id, self.comp_contrib)
                compounds.append(compound)
            except KeyError as e:
                logging.warning(str(e))

        lbs, ubs = self.bounds.get_ln_bounds(compounds)
        lnC_dict = {
            met_id: Variable(f"M@lnC@{met_id}", lb=lb, ub=ub)
            for met_id, lb, ub in zip(stoich_df.metabolite_id, lbs, ubs)
        }

        df = stoich_df.join(pd.Series(lnC_dict, name="lnC"), on="metabolite_id")

        # Then we calculate the reaction potentials (by multiplying the
        # the metabolite potential vector by S).
        df["lnC_times_coeff"] = df.lnC.multiply(df.coefficient)

        # The precalculated dG'0 values are in units of RT. We also ignore
        # all those that have a high uncertainty.
        thermo_df = self.standard_dg[
            ~pd.isnull(self.standard_dg.standard_dg_prime)
            & (self.standard_dg.standard_dg_prime_error < 1000)
        ]

        fwd_df = thermo_df.copy()
        fwd_df["reaction_id"] = thermo_df.bigg_id + "_F"
        fwd_df.set_index("reaction_id", inplace=True)

        rev_df = thermo_df.copy()
        rev_df["reaction_id"] = thermo_df.bigg_id + "_R"
        rev_df.set_index("reaction_id", inplace=True)
        rev_df["standard_dg_prime"] *= -1.0

        thermo_df = pd.concat([fwd_df, rev_df])
        df = df.join(thermo_df, on="reaction_id", how="inner")

        large_energy_value = 1000.0

        # the margin is in units of RT
        mdf_lb_over_rt = (self.mdf_lb / self.comp_contrib.RT).m_as("")
        margin = Variable(
            "MDF_margin", lb=mdf_lb_over_rt, ub=large_energy_value, type="continuous"
        )
        logging.debug("Adding thermodynamic constraints")
        self.lp_model.add([margin])
        self.lp_model.update()
        for irrev_rxn_id, group_df in df.groupby("reaction_id"):
            standard_dg_prime = group_df.standard_dg_prime.iat[0]
            gamma = group_df.gamma.iat[0]
            dg_prime = float(standard_dg_prime) + (
                symbolics.add(group_df.lnC_times_coeff.values)
            )

            # Add the constraints on dG', i.e.:
            #        margin <= -dG'/RT + M * (1-gamma)
            #
            # which translates to:
            #        margin + M*gamma + dG'/RT <= M
            cnstr = Constraint(
                margin + large_energy_value * gamma + dg_prime,
                lb=-large_energy_value,
                ub=large_energy_value,
                name=f"R@thermodynamic@{irrev_rxn_id}",
            )
            self.lp_model.add([cnstr])
        self.lp_model.update()

        return margin

    def exclude_solution(self, solution: Solution) -> None:
        """Exclude a solution from the LP.

        :param solution: the solution to exclude
        :return:
        """

        # for each previous solution, add constraints on the indicator
        # variables, so that that solution will not repeat (i.e. the sum
        # over the previous reaction set must be less than the size of
        # that set).
        logging.debug("Adding constraints: exclude previous solutions")

        sum_of_objective_coefficients = 0.0
        weighted_sum_of_gammas = []
        for var_name in solution.active_indicators:
            gamma = self.lp_model.variables[var_name]
            tokens = re.findall(r"R@gamma@(\w+)_([FR])", var_name)
            assert len(tokens) == 1, var_name
            reaction_id, direction = tokens[0]
            objective_coefficient = self.get_objective_coefficient(reaction_id)
            if objective_coefficient > 0:
                weighted_sum_of_gammas.append(gamma * objective_coefficient)
                sum_of_objective_coefficients += objective_coefficient

        weighted_sum_of_gammas = symbolics.add(weighted_sum_of_gammas)
        self.lp_model.add(
            Constraint(
                weighted_sum_of_gammas,
                lb=None,
                ub=sum_of_objective_coefficients - self.search_radius - 0.5,
                name=f"G@exclude_solution@{solution.solution_id:03d}",
            )
        )

    def write_lp(self, lp_path):
        """write the LP to this file (only for debugging)."""
        logging.debug(f"Writing LP to {lp_path}")
        with open(lp_path, "wt") as fp:
            json.dump(self.lp_model.to_json(), fp, indent="    ")

    def get_next_solution(self, counter: int) -> Solution | None:
        """Solve the LP and find the next shortest path.

        :return: A solution or None
        """
        logging.debug("Optimizing MILP")
        if self.lp_model.optimize() != "optimal":
            return None

        if "MDF_margin" in self.lp_model.variables:
            mdf = self.lp_model.variables["MDF_margin"].primal * self.comp_contrib.RT
        else:
            mdf = Q_("0 kJ/mol")

        var_df = pd.DataFrame(
            data=[
                (var_name, var.primal, var.lb, var.ub)
                for var_name, var in self.lp_model.variables.items()
            ],
            columns=["name", "primal", "lb", "ub"],
        )
        var_df["lp_type"] = "variable"

        cnstr_df = pd.DataFrame(
            data=[
                (cnstr_name, cnstr.primal, cnstr.lb, cnstr.ub)
                for cnstr_name, cnstr in self.lp_model.constraints.items()
            ],
            columns=["name", "primal", "lb", "ub"],
        )
        cnstr_df["lp_type"] = "constraint"

        return Solution(
            counter,
            self.cobra_model,
            self.cofactor_set,
            pd.concat([var_df, cnstr_df], axis=0),
            mdf,
        )

    def verify_feasibility(self):
        """Use standard LP to ensure the solution space is not empty."""

    @staticmethod
    def initialize_standard_ecoli_model(
        pathway_objective: Dict[str, float],
        conc_bounds: Bounds,
        cobra_model: cobra.Model,
        comp_contrib: ComponentContribution,
    ) -> "PathFinder":

        ################################################################################
        # make all the reactions in the model reversible, except for all reactions
        # that are boundary (e.g. dummy reactions), non-cytoplasmic
        for rxn in cobra_model.reactions:
            if rxn.boundary or rxn.compartments != {"c"}:
                rxn.knock_out()
            else:
                rxn.bounds = (-10, 10)

        ################################################################################
        # Replace Flavoredoxin and Thioredoxin with NADP(H) and remove the
        # reactions transferring electrons between them. This is a way to deal with
        # the uncertainty in the thermodynamics of these electron carriers (leaving them
        # usually causes the pathway search to find "easy" solutions that are probably
        # not realistic.
        thioredoxin = cobra_model.metabolites.trdrd_c
        for r in thioredoxin.reactions:
            if r.get_coefficient(thioredoxin) == -1:
                r += cobra_model.reactions.TRDR
            elif r.get_coefficient(thioredoxin) == 1:
                r -= cobra_model.reactions.TRDR

        flavoredoxin = cobra_model.metabolites.flxr_c
        for r in flavoredoxin.reactions:
            if r.get_coefficient(flavoredoxin) == -2:
                r += cobra_model.reactions.FLDR2
            elif r.get_coefficient(flavoredoxin) == 2:
                r -= cobra_model.reactions.FLDR2

        cobra_model.reactions.TRDR.knock_out()  # Thioredoxin reductase (NADPH)
        cobra_model.reactions.FLDR2.knock_out()  # Flavodoxin reductase (NADPH)

        # remove formate-THF ligase was mistakenly included in iML1515
        cobra_model.reactions.FTHFLi.knock_out()

        ################################################################################

        comp_contrib.p_h = PathFinder.DEFAULT_P_H
        comp_contrib.ionic_strength = PathFinder.DEFAULT_IONIC_STRENGTH
        comp_contrib.p_mg = PathFinder.DEFAULT_P_MG

        ###############################################################################

        cf = PathFinder(cobra_model, conc_bounds, comp_contrib)
        cf.set_pathway_objective(pathway_objective)

        cf.add_reaction(
            "__regenerate_NADPH__",
            name="regenerate NADPH from NADP+",
            metabolites_to_add={"nadp_c": -1, "nadph_c": 1},
            lower_bound=-10,
            upper_bound=10,
            objective_coefficient=0.0,
        )

        cf.add_reaction(
            "__regenerate_NADH__",
            name="regenerate NADH from NAD+",
            metabolites_to_add={"nad_c": -1, "nadh_c": 1},
            lower_bound=-10,
            upper_bound=10,
            objective_coefficient=0.0,
        )

        cf.add_reaction(
            "__regenerate_ATP__",
            name="regenerate ATP from ADP",
            metabolites_to_add={"adp_c": -1, "atp_c": 1, "h2o_c": 1, "pi_c": -1},
            lower_bound=-10,
            upper_bound=10,
            objective_coefficient=0.0,
        )

        protons = list(map(cf.cobra_model.metabolites.get_by_id, ["h_c", "h_p", "h_e"]))
        cf.cobra_model.remove_metabolites(protons)
        for free_met in ["h2o_c", "pi_c", "nh4_c", "o2_c"]:
            cf.add_reaction(
                id=f"__regenerate_{free_met}__",
                name=f"regenerate inorganic compound {free_met}",
                metabolites_to_add={free_met: 1},
                lower_bound=-10,
                upper_bound=10,
                objective_coefficient=0.0,
            )
        return cf

    def search(
        self,
    ) -> List[Solution]:
        """Find all the n-shortest paths that satisfy the constraints.
        :return: a list of Solution objects
        """
        solutions = []

        logging.debug(
            f"Looking for pathways using the COBRA model '{self.cobra_model.id}'. "
            f"Stopping after {self.max_iterations} solutions. Writing solutions to "
            f"{self.result_pathway_path}."
        )
        self.create_min_step_lp()

        # Use iterative MILP to find the n-best optimal solutions
        for counter in tqdm(range(self.max_iterations), desc="searching for pathways"):
            solution = self.get_next_solution(counter)

            if solution is None:
                if counter == 0:
                    logging.warning("Couldn't find any solutions")
                else:
                    logging.info(f"Couldn't find more than {counter} solutions")
                return solutions
            solutions.append(solution)
            self.exclude_solution(solution)

        logging.debug(f"Stopping after {self.max_iterations} solutions. More might exist.")
        return solutions

    def write_solution_sbtabs(self, solutions: List[Solution]) -> None:
        self.result_pathway_dir.mkdir(exist_ok=True)
        data = []
        for i, solution in enumerate(solutions):
            sbtab_path = str(self.result_pathway_dir / f"pathway_{i:03d}.csv")
            solution.to_sbtab().write(sbtab_path)
            data.append(
                {
                    "pathway_name": f"pathway_{i:03d}",
                    "pathway_sbtab": sbtab_path,
                    "n_reactions": solution.n_reactions,
                    "mdf": solution.mdf,
                    "total_flux": solution.total_flux,
                }
            )
        summary_df = pd.DataFrame(data)
        summary_df.to_csv(self.result_dir / "pathway_summary.csv")

    def draw_solution_graphs(self, solutions: List[Solution]) -> None:
        self.result_gdot_dir.mkdir(exist_ok=True)
        for i, solution in enumerate(solutions):
            g_dot, r_nodes, m_nodes = solution.to_graph()
            g_dot.write_pdf(self.result_gdot_dir / f"pathway_{i:03d}.pdf")

    def read_pathway_summary(self) -> pd.DataFrame:
        return pd.read_csv(self.result_dir / "pathway_summary.csv", index_col=0)

    def create_ecm_configuration_files(self) -> None:
        """use ENKIE to populate the models with kinetic parameters and write the
        configuration files for ECM.
        """
        self.result_enkie_dir.mkdir(exist_ok=True)
        self.result_ecm_dir.mkdir(exist_ok=True)

        summary_df = self.read_pathway_summary()
        if summary_df.shape[0] == 0:
            return

        ecm_data = []
        for row in tqdm(
            summary_df.itertuples(),
            desc="Running ENKIE on each pathway",
            total=summary_df.shape[0],
        ):
            sbtabdoc = SBtab.read_csv(row.pathway_sbtab, document_name=row.pathway_name)
            try:
                parameter_space, ecm_sbtabdoc = kinetics.add_ecm_params_to_sbtab(
                    sbtabdoc=sbtabdoc,
                    cobra_model=self.cobra_model,
                    comp_contrib=self.comp_contrib,
                    bounds=self.bounds,
                )
                parameter_space.mean.to_csv(
                    str(self.result_enkie_dir / f"{row.pathway_name}_parameters_mean.csv")
                )

                sbtab_path = str(self.result_ecm_dir / f"{row.pathway_name}_ecm.tsv")
                ecm_sbtabdoc.write(sbtab_path)
                ecm_data.append(
                    {"pathway_name": row.pathway_name, "ecm_config_sbtab": sbtab_path}
                )
            except Exception as e:
                print("Error while adding parameters using ENKIE: " + str(e))
                ecm_data.append(
                    {"pathway_name": row.pathway_name, "enkie_error_msg": str(e)}
                )

        summary_df.drop(
            ["ecm_config_sbtab", "enkie_error_msg"],
            axis=1,
            inplace=True,
            errors="ignore",
        )
        summary_df = pd.merge(summary_df, pd.DataFrame(ecm_data), on="pathway_name")
        summary_df.to_csv(self.result_dir / "pathway_summary.csv")

    def run_mdf(self) -> None:
        """Run the MDF analysis on all the saved pathways."""
        summary_df = self.read_pathway_summary()

        if "ecm_config_sbtab" not in summary_df.columns:
            raise ValueError(
                "you must run `create_ecm_configuration_files()` before " "`run_mdf()`."
            )

        result_data = []
        for row in tqdm(
            summary_df.itertuples(),
            desc="Running MDF on each pathway",
            total=summary_df.shape[0],
        ):
            try:
                model = ThermodynamicModel.from_sbtab(
                    str(row.ecm_config_sbtab), comp_contrib=self.comp_contrib
                )
                model.update_standard_dgs()
                mdf_sol = model.mdf_analysis()
                result_path = str(self.result_ecm_dir / f"{row.pathway_name}_mdf_results.tsv")
                mdf_sol.to_sbtab().write(result_path)

                fig, ax = plt.subplots(1, 1, figsize=(7, 4), dpi=120)
                ax.set_title("MDF solution")
                mdf_sol.plot_driving_forces(ax)
                fig.tight_layout()
                fig.savefig(self.result_ecm_dir / f"{row.pathway_name}_mdf_profile.pdf")
                plt.close(fig)

                result_data.append(
                    {
                        "pathway_name": row.pathway_name,
                        "mdf": mdf_sol.score,
                        "mdf_result_sbtab": result_path,
                    }
                )

            except Exception as e:
                result_data.append(
                    {
                        "pathway_name": row.pathway_name,
                        "mdf_error_message": str(e),
                    }
                )

        summary_df.drop(
            ["mdf", "mdf_result_sbtab", "mdf_error_message"],
            axis=1,
            inplace=True,
            errors="ignore",
        )
        summary_df = pd.merge(summary_df, pd.DataFrame(result_data), on="pathway_name")
        summary_df.to_csv(self.result_dir / "pathway_summary.csv")

    def run_ecm(self) -> None:
        """Run the ECM analysis on all the saved pathways."""
        summary_df = self.read_pathway_summary()

        if "ecm_config_sbtab" not in summary_df.columns:
            raise ValueError(
                "you must run `create_ecm_configuration_files()` before " "`run_ecm()`."
            )

        result_data = []

        for row in tqdm(
            summary_df.itertuples(),
            desc="Running ECM on each pathway",
            total=summary_df.shape[0],
        ):
            if not row.ecm_config_sbtab:
                result_data.append(
                    {
                        "pathway_name": row.pathway_name,
                        "ecm_error_message": "no ECM config file found",
                    }
                )
                continue

            model = EnzymeCostModel.from_sbtab(
                str(row.ecm_config_sbtab), comp_contrib=self.comp_contrib
            )
            try:
                ecm_sol = model.optimize_ecm()
                result_path = str(self.result_ecm_dir / f"{row.pathway_name}_ecm_results.tsv")
                ecm_sol.to_sbtab().write(result_path)

                fig, ax = plt.subplots(1, 1, figsize=(7, 4), dpi=120)
                ax.set_title("ECM solution")
                ecm_sol.plot_enzyme_demand_breakdown(ax)
                fig.tight_layout()
                fig.savefig(self.result_ecm_dir / f"{row.pathway_name}_ecm_costs.pdf")
                plt.close(fig)

                result_data.append(
                    {
                        "pathway_name": row.pathway_name,
                        "ecm": ecm_sol.score,
                        "ecm_result_sbtab": result_path,
                    }
                )

            except Exception as e:
                result_data.append(
                    {
                        "pathway_name": row.pathway_name,
                        "ecm": np.nan,
                        "ecm_error_message": str(e),
                    }
                )

        summary_df.drop(
            ["ecm", "ecm_result_sbtab", "ecm_error_message"],
            axis=1,
            inplace=True,
            errors="ignore",
        )
        summary_df = pd.merge(summary_df, pd.DataFrame(result_data), on="pathway_name")
        summary_df.to_csv(self.result_dir / "pathway_summary.csv")

    def plot_pareto(self) -> plt.Figure:
        """Plot the Pareto optimality scatter plot for all the pathways."""
        summary_df = self.read_pathway_summary()
        if "ecm_config_sbtab" not in summary_df.columns:
            raise ValueError(
                "you must run `create_ecm_configuration_files()` and "
                "`run_ecm()` before `plot_pareto()`."
            )
        if "ecm" not in summary_df.columns:
            raise ValueError("you must run `run_ecm()` before `plot_pareto()`.")

        fig, ax = plt.subplots(1, 1, figsize=(5, 5), dpi=200)
        ax.scatter(summary_df.mdf, summary_df.ecm, s=20, color=(0.9, 0.6, 0.6))
        for row in summary_df.itertuples():
            if pd.isnull(row.mdf) or pd.isnull(row.ecm):
                continue
            ax.text(
                row.mdf, row.ecm, str(row.Index), ha="center", va="center", zorder=2
            )
        ax.set_yscale("log")
        ax.set_xlabel("MDF [kJ/mol]")
        ax.set_ylabel("ECM [gr/l]")
        ax.set_xlim(0, None)
        fig.savefig(self.result_dir / f"pareto_mdf_vs_ecm.pdf")
        return fig
