# The MIT License (MIT)

# Copyright (c) 2020 Weizmann Institute of Science
# Copyright (c) 2020 Elad Noor

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
import os
from typing import Dict, Iterable, List, Tuple
from warnings import warn

import cobra
import numpy as np
import pandas as pd
from equilibrator_api import Q_, ComponentContribution, Reaction
from equilibrator_api.model import Bounds
from equilibrator_cache import Compound

from path import Path

BIGG2EXTERNAL = {
    "nadphx__S_c": "CHEBI:64076",
    "nadphx__R_c": "CHEBI:64085",
    "nadhx__S_c": "CHEBI:44236",
    "nadhx__R_c": "CHEBI:134125",
    "micit_c": "CHEBI:15607",
    # "ttdca_c": "metanetx.chemical:MNXM314",
    # "cdpdhdecg_c": "CHEBI:77190",
    # "cdpdddecg_c": "metanetx.chemical:MNXM4507",
    # "hxcoa_c": "CHEBI:62620",
    # "dkdgln_c": "ChEBI:15623",
    # "dt5hsu_c": "CHEBI:17117",
    # "dxylnt_c": "CHEBI:48093",
    # "2ddara_c": "CHEBI:16699",
    # "urdgci_c": "CHEBI:16282",
    # "metglcur_c": "CHEBI:183134",
    # "psuri_c": "CHEBI:17802",
    # "psuri_e": "CHEBI:17802",
    # "psuri_p": "CHEBI:17802",
    # "f_c": "CHEBI:17051",
    # "f_p": "CHEBI:17051",
    # "sq_c": "CHEBI:77132",
    # "sf_c": "CHEBI:77261",
    # "sfp_c": "CHEBI:77263",
    # "sla_c": "CHEBI:77265",
    # "sq_p": "CHEBI:77132",
    # "dhptdp_c": "KEGG:C20959",
    # "dhptdd_c": "KEGG:C20960",
    # "dtgcl_c": "CHEBI:43136",
    # "mercpeth_c": "CHEBI:41218",
    # "dhcholn_c": "CHEBI:16390",
    # "4abzglu_c": "CHEBI:60903",
    # "5phdt_c": "CHEBI:140750",
    # "5phua_c": "CHEBI:140758",
    # "teo2_c": "CHEBI:30477",
    # "mteo2_c": "CHEBI:71624",
    # "34dphacoa_c": "CHEBI:189558",
    # "mchtbs6p_c": "CHEBI:71317",
    # "cur_c": "CHEBI:3962",
    # "dhcur_c": "CHEBI:67262",
    # "lkdr_c": "CHEBI:58371",
    # "acmet_c": "CHEBI:132958",
    # "iprimv_c": "CHEBI:146431",
    # "erthrs_c": "CHEBI:27904",
    # "puacgam_c": "CHEBI:67063",
    # "dhps_c": "KEGG:C22383",
    # "dhps_e": "KEGG:C22383",
    # "cs1_e": "CHEBI:49547",
    # "r2hglut_c": "CHEBI:15801",
    # "prpmn_c": "KEGG:C20423",
    # "prcp_c": "KEGG:C20440",
    # "gmplys_c": "CHEBI:64856",
    # "nalme_c": "CHEBI:64854",
    # "mepn_c": "CHEBI:45129",
    # "rpntp_c": "CHEBI:68685",
    # "udcpgl_c": "KEGG:C19772",
    # "4abzglu_e": "CHEBI:60903",
    # "metglcur_e": "CHEBI:183134",
    # "metglcur_p": "CHEBI:183134",
    # "thcur_c": "CHEBI:67263",
    # "rhmn_c": "CHEBI:58118",
    # "3hbzcoa_c": "CHEBI:15484",
    # "3hbz_c": "CHEBI:16193",
    # "ampnt_c": "CHEBI:133674",
    # "acampnt_c": "CHEBI:134093",
    # "dkdglcn_c": "CHEBI:15623",
    # "2hptcoa_c": None,  # not found in KEGG or CHEBI
    # "2hptcl_c": None,  # not found in KEGG or CHEBI
    # "cxsam_c": None,  # not found in KEGG or CHEBI
    # "sqg_c": None,  # not found in KEGG or CHEBI
}


def cobra_met_to_equilibrator(
        met_id: str, comp_contrib: ComponentContribution
) -> Tuple[str, Compound]:
    assert isinstance(met_id, str)

    if met_id in BIGG2EXTERNAL:
        extern_met_id = BIGG2EXTERNAL[met_id]
        return extern_met_id, comp_contrib.get_compound(extern_met_id)

    try:
        # we drop the BiGG compartment suffix (e.g. "_c")
        miriam_id = f"bigg.metabolite:{met_id[0:-2]}"
        return miriam_id, comp_contrib.get_compound(miriam_id)
    except Exception as e:
        logging.warning(f"Error when searching for reactant {met_id}", str(e))
        return None, None


def cobra_rxn_to_equilibrator(
        rxn: cobra.Reaction, comp_contrib: ComponentContribution
) -> Tuple[Reaction | None, Dict[Compound, str]]:
    """Convert a cobra.Reaction to equilibrator_api.Reaction

    :param rxn: a cobra.Reaction
    :return: (reaction, compound_name_mapping)
    """
    compound_name_mapping = {}
    sparse = {}
    for met, coeff in rxn.metabolites.items():
        met_id, cpd = cobra_met_to_equilibrator(met.id, comp_contrib)
        if cpd is None:
            raise ValueError(
                f"cannot balance this reaction because the metabolite "
                f"{met.id} was not found in equilibrator-cache"
            )

        compound_name_mapping[cpd] = met_id
        sparse[cpd] = coeff
    eq_rxn = Reaction(sparse, rid=rxn.id)

    # if necessary, balance the reaction with water molecules (which are
    # commonly missing in BiGG models)
    balanced_eq_rxn = eq_rxn.balance_with_compound(
        comp_contrib.get_compound("bigg.metabolite:h2o"),
        ignore_atoms=("H",),
    )
    if balanced_eq_rxn is None:
        warn(f"reaction cannot be balanced: {rxn.build_reaction_string()}")
        return eq_rxn, compound_name_mapping
    else:
        return balanced_eq_rxn, compound_name_mapping


def bigg_id_to_equilibrator(
        cobra_model: cobra.Model,
        reaction_ids: Iterable[str],
        comp_contrib: ComponentContribution,
) -> Tuple[List[Reaction], Dict[Compound, str]]:
    """Convert a list of reactions to equilibrator_api.Reaction

    :param reaction_ids: a list of BiGG reaction IDs
    :return: (reactions, compound_name_mapping) where the former is a
    list of equilibrator_api.Reaction object, and the latter is a
    dictionary for mapping Compounds to the original BiGG IDs (useful for
    writing the reactions as text).
    """
    compound_name_mapping = {}
    reactions = []
    for reaction_id in reaction_ids:
        rxn = cobra_model.reactions.get_by_id(reaction_id)
        try:
            eq_rxn, mapping = cobra_rxn_to_equilibrator(rxn, comp_contrib)
            reactions.append(eq_rxn)
            compound_name_mapping.update(mapping)
        except ValueError as e:
            warn(f"Cannot convert reaction {reaction_id} to " f"eQuilibrator: {str(e)}")
            reactions.append(Reaction({}))
    return reactions, compound_name_mapping


def calculate_standard_dgs(
        cobra_model: cobra.Model, comp_contrib: ComponentContribution
) -> pd.DataFrame:
    """Calculate the standard dG' values fo an entire COBRA model."""
    p_h = comp_contrib.p_h.m_as("")
    i_s = comp_contrib.ionic_strength.m_as("M")
    p_mg = comp_contrib.p_mg.m_as("")
    thermo_fname = f"thermo_{cobra_model.id}_ph{p_h:.2f}_I{i_s:.2f}_pmg{p_mg:.2f}.csv"
    if os.path.exists(thermo_fname):
        logging.info(f"Found thermodynamics for {cobra_model.id}")
        standard_dg = pd.read_csv(thermo_fname, header=0, index_col=0)
    else:
        logging.info(
            f"Couldn't find a cache file {thermo_fname}. Calculating "
            f"thermodynamics for {cobra_model.id}"
        )
        standard_dg_data = []
        for cobra_reaction in cobra_model.reactions:
            if cobra_reaction.id[0:2] == "__":
                # skip manually added 'fake' reactions
                continue
            if cobra_reaction.compartments != {"c"}:
                # skip non-cytoplasmic reactions
                value = np.nan
                error = np.nan
                comment = "reaction is not purely in the cytoplasm"
            else:
                try:
                    eq_rxn, _ = cobra_rxn_to_equilibrator(cobra_reaction, comp_contrib)
                    std_dg_prime = comp_contrib.standard_dg_prime(eq_rxn)
                    std_dg_prime /= comp_contrib.RT  # convert to unitless
                    value = std_dg_prime.value.m_as("")
                    error = std_dg_prime.error.m_as("")
                    comment = ""
                except Exception as e:
                    value = np.nan
                    error = np.nan
                    comment = str(e)

            standard_dg_data.append((cobra_reaction.id, value, error, comment))

        standard_dg = pd.DataFrame(
            data=standard_dg_data,
            columns=[
                "bigg_id",
                "standard_dg_prime",
                "standard_dg_prime_error",
                "comment",
            ],
        )
        standard_dg.to_csv(thermo_fname)
        logging.info(f"Saving thermodynamics to {thermo_fname}")

    return standard_dg


def read_compound_bounds(
        bounds_path: str | Path,
        comp_contrib: ComponentContribution,
) -> Bounds:
    """Read the concentration bounds for compounds from a CSV file."""
    bounds_df = pd.read_csv(bounds_path)
    bounds_df["compound"] = bounds_df["Compound:Identifiers"].apply(
        comp_contrib.get_compound
    )
    bounds_df = bounds_df.set_index("compound")
    bounds_df["lb"] = bounds_df["Concentration:Min"].apply(Q_)
    bounds_df["ub"] = bounds_df["Concentration:Max"].apply(Q_)
    default_conc_lb = Q_("1e-6 M")
    default_conc_ub = Q_("1e-2 M")
    conc_bounds = Bounds(
        bounds_df.lb.to_dict(), bounds_df.ub.to_dict(), default_conc_lb, default_conc_ub
    )
    conc_bounds.check_bounds()
    return conc_bounds


def get_dg_range(
        rxn: Reaction, bounds: Bounds, comp_contrib: ComponentContribution
) -> Tuple[Q_, Q_]:
    """Calculate the range of possible Gibbs energies for a given reaction."""

    standard_dg_prime = comp_contrib.standard_dg_prime(rxn)
    compounds = rxn.sparse.keys()

    cpd_to_ln_lb = dict(zip(compounds, bounds.get_ln_lower_bounds(compounds)))
    cpd_to_ln_ub = dict(zip(compounds, bounds.get_ln_upper_bounds(compounds)))

    min_dg_prime = standard_dg_prime.value + comp_contrib.RT * sum([
        coeff * cpd_to_ln_ub[cpd] if coeff < 0 else coeff * cpd_to_ln_lb[cpd]
        for cpd, coeff in rxn.items(protons=False, water=False)
    ])
    max_dg_prime = standard_dg_prime.value + comp_contrib.RT * sum([
        coeff * cpd_to_ln_lb[cpd] if coeff < 0 else coeff * cpd_to_ln_ub[cpd]
        for cpd, coeff in rxn.items(protons=False, water=False)
    ])

    return min_dg_prime, max_dg_prime
