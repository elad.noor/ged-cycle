# equilibrator-designer

## Are you looking for the GED cycle source code?
If you are looking for the source code from the paper
["A latent carbon fixation cycle in *E. coli* mirrors the Calvin Cycle" (Nature Comm. 2020)](https://doi.org/10.1038/s41467-020-19564-5), 
please follow [this link](https://gitlab.com/elad.noor/ged-cycle/-/tree/9c3c56c08af9db6bf5bac895e1da6caba1f33276)
to a tagged version of the repository which was relevant for that time. Note that this repository has continued to develop
since, although most of the core functionality has remained the same.

## What is *equilibrator-designer*?
A pipeline for automatic design and analysis of novel
pathways using native *E. coli* enzymes

All the user needs to do is define a goal function, for example
```{"co2_c": -3, "pyr_c": 1}``` searches for pathways that produce
1 mole of pyruvate from 3 moles of CO2 (while making use of pre-defined co-factors).
The pipeline generates several solutions and ranks them automatically using the 
Max-min Driving-Force (MDF) and Enzyme-Cost Minimization (ECM) algorithms.


### Requirements:
- python 3.9+
- numpy
- scipy
- matplotlib
- pandas
- sbtab
- cobra
- optlang
- biopython
- MILP solver: CPLEX or Gurobi
- equilibrator-api (0.6.0)
- equilibrator-cache (0.6.0)
- ENKIE

For plotting:
- pydot
- graphviz
- gravis
- seaborn
- jupyterlab


