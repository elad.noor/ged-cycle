# The MIT License (MIT)

# Copyright (c) 2020 Weizmann Institute of Science
# Copyright (c) 2020 Elad Noor

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import uuid
from typing import Dict, Iterable, Set, TextIO, Tuple

import cobra
import numpy as np
import pandas as pd
import pydot
import gravis
from equilibrator_api import Q_
from sbtab.SBtab import SBtabDocument, SBtabTable


class Solution(object):
    """Contained class for path solutions."""

    def __init__(
        self,
        solution_id: int,
        cobra_model: cobra.Model,
        cofactor_set: Set[cobra.Metabolite],
        solution_df: pd.DataFrame,
        mdf: Q_,
    ):
        """
        :param solution_id: a unique index used to identify solutions
        :param cobra_model: the cobra Model used to generate this solution
        :param cofactor_set: the set of cofactors (for visualizing the path)
        :param solution_df: a DataFrame with all the variable and constraint
        data
        :param mdf: the Max-min Driving Force
        """
        self.solution_id = solution_id
        self.name = f"pathway_{self.solution_id:03d}"
        self.cobra_model = cobra_model
        self.cofactor_set = cofactor_set
        self.df = solution_df

        self._mdf = mdf
        assert mdf.check("[energy]/[substance]"), f"MDF units are wrong: {mdf}"
        self._mdf.ito("kJ/mol")

        # construct a specific DataFrame for all the varaibles, and split the
        # name to the 3 parts so that we can use the BiGG IDs in the model later
        self.var_df = solution_df[solution_df.lp_type == "variable"].copy()
        tmp = self.var_df.name.str.split("@", n=3, expand=True)
        self.var_df["bigg_id_type"] = tmp[0]
        self.var_df["type"] = tmp[1]
        self.var_df["bigg_id"] = tmp[2]

        # for reactions, we need to map the 'split' IDs back to the original
        # BiGG identifiers (i.e. reversible reactions)
        flux_df = self.var_df[
            (self.var_df.type == "flux")
            & (self.var_df.bigg_id.str[0:2] != "__")
            & (self.var_df.primal.abs() > 1e-3)
        ].copy()
        tmp = flux_df.bigg_id.str.rsplit("_", n=1, expand=True)
        flux_df["reaction_id"] = tmp[0]
        flux_df["direction"] = tmp[1]
        flux_df.loc[flux_df.direction == "R", "primal"] *= -1.0
        flux_df.drop(["direction", "bigg_id", "bigg_id_type"], axis=1)
        flux_df = flux_df.groupby(["reaction_id", "type"]).sum().reset_index()
        flux_df["reaction"] = flux_df.reaction_id.apply(
            self.cobra_model.reactions.get_by_id
        )
        flux_df["formula"] = flux_df.reaction.apply(lambda r: r.build_reaction_string())

        self.flux_df = flux_df.round(3)

    @property
    def total_flux(self) -> float:
        """The sum of all fluxes"""
        return self.flux_df.primal.abs().sum()

    @property
    def n_reactions(self) -> int:
        """The number of distinct reactions."""
        return self.flux_df.shape[0]

    @property
    def mdf(self) -> Q_:
        """The MDF values"""
        return self._mdf

    @property
    def active_indicators(self) -> Iterable[str]:
        return self.var_df[
            (self.var_df.type == "gamma") & (self.var_df.primal == 1)
        ].name

    def write_to_file(self, handle: TextIO) -> None:
        """Write the solution to a KEGG-style file"""

        handle.write(f"ENTRY       M-{self.name.upper()}\n")
        handle.write("SKIP        FALSE\n")
        handle.write(f"NAME        M-{self.name.upper()}\n")
        handle.write("TYPE        MARGIN\n")
        handle.write("CONDITIONS  pH=7.5,I=0.3,T=300\n")
        handle.write("C_MID       0.0001\n")

        first = True
        for row in self.flux_df.itertuples():
            if first:
                prefix = "REACTION"
                first = False
            else:
                prefix = ""
            handle.write(f"{prefix:12s}{row.formula} (x{row.primal})\n")

        handle.write("///\n")
        handle.flush()

    def get_stats_df(self) -> pd.DataFrame:
        """Return a DataFrame with all the statistics of this solution."""
        mdf_unitless = np.round((self.mdf / Q_("kJ/mol")).magnitude, 2)
        stats_df = pd.DataFrame(
            columns=["!ID", "!QuantityType", "!Unit", "!Value"],
            data=[
                ("mdf", "max-min driving force", "kJ/mol", mdf_unitless),
                ("nr", "number of reactions", "dimensionless", self.n_reactions),
                ("sum_flux", "sum of fluxes", "dimensionless", self.total_flux),
            ],
        )
        return stats_df

    def to_sbtab(self) -> SBtabDocument:
        """Return an SBtabDocument containing the solution."""
        sbtabdoc = SBtabDocument("pathway", filename="pathway.tsv")

        sb_reaction_df = (
            self.flux_df[["reaction_id", "formula"]]
            .astype(str)
            .rename(columns={"reaction_id": "!ID", "formula": "!ReactionFormula"})
        )
        reaction_sbtab = SBtabTable.from_data_frame(
            sb_reaction_df,
            table_id=f"Reaction",
            table_type="Reaction",
        )
        sbtabdoc.add_sbtab(reaction_sbtab)

        sb_flux_df = (
            self.flux_df[["reaction_id", "primal"]]
            .astype(str)
            .rename(columns={"reaction_id": "!Reaction", "primal": "!Value"})
        )
        sb_flux_df.insert(loc=0, column="!QuantityType", value="rate of reaction")
        flux_sbtab = SBtabTable.from_data_frame(
            sb_flux_df,
            table_id=f"Flux",
            table_type="Quantity",
        )
        flux_sbtab.change_attribute("Unit", "mM/s")
        sbtabdoc.add_sbtab(flux_sbtab)

        stats_sbtab = SBtabTable.from_data_frame(
            self.get_stats_df().astype(str),
            table_id=f"Statistics",
            table_type="Quantity",
        )
        sbtabdoc.add_sbtab(stats_sbtab)
        return sbtabdoc

    def create_metabolite_node(self, met: cobra.Metabolite) -> pydot.Node:
        """Create a graphviz Node for a Metabolite.

        :param met: A Metabolite
        :param cofactor_set: the set of all co-factor metabolites
        :return: a pydot.Node object
        """
        node = pydot.Node(name=str(uuid.uuid1()))
        node.set("label", f'"{met.id}"')
        node.set("tooltip", met.name)
        node.set("fontsize", "8")
        node.set("fontname", "verdana")
        node.set("style", "filled")

        url = (
            f'"http://bigg.ucsd.edu/universal/metabolites/'
            f"{met.id.rsplit('_', 1)[0]}\""
        )
        if met.id[0:2] == "__":
            node.set("shape", "tripleoctagon")
            node.set("fontcolor", "darkgreen")
            node.set("fillcolor", "white")
        elif met in self.cofactor_set:
            node.set("shape", "plaintext")
            node.set("URL", url)
            node.set("fontcolor", "dodgerblue")  # color for cofactors
            node.set("fillcolor", "white")
        else:
            node.set("shape", "octagon")
            node.set("URL", url)
            node.set("fontcolor", "white")  # color for non-cofcators
            node.set("fillcolor", "dodgerblue")
        return node

    @staticmethod
    def create_reaction_node(rxn: cobra.Reaction) -> pydot.Node:
        """Create a graphviz node for a Reaction.

        :param rxn: a Reaction
        :return: a pydot.Node object
        """
        node = pydot.Node(name=str(uuid.uuid1()))
        node.set("label", f'"{rxn.id}"')
        node.set("tooltip", rxn.id)
        node.set("URL", f'"http://bigg.ucsd.edu/universal/reactions/{rxn.id}"')
        node.set("shape", "oval")
        node.set("style", "filled")
        node.set("fontcolor", "darkgreen")
        node.set("fillcolor", "white")
        node.set("fontsize", "5")
        node.set("fontname", "verdana")
        return node

    @staticmethod
    def create_edge(
        m_node: pydot.Node, r_node: pydot.Node, weight: float = 1.0, flux: float = 1.0
    ) -> pydot.Edge:
        """Create a graphviz edge between a metabolite and a reaction.

        :param m_node: A Metabolite Node
        :param r_node: A Reaction Mode
        :param weight: the graphviz weight for this edge
        :param flux: the flux going through this reaction
        :return: A pydot.Edge object
        """

        if flux < 0:
            edge = pydot.Edge(m_node, r_node)
        else:
            edge = pydot.Edge(r_node, m_node)

        if abs(abs(flux) - 1.0) > 1e-3:
            edge.set("label", f'"{abs(flux):.3g}"')

        edge.set("arrowhead", "open")
        edge.set("arrowtail", "none")
        edge.set("color", "cadetblue")  # edge line color
        edge.set("fontcolor", "indigo")  # edge label color
        edge.set("fontname", "verdana")
        edge.set("fontsize", "8")
        edge.set("weight", weight)
        return edge

    def to_graph(
        self,
    ) -> Tuple[
        pydot.Dot, Dict[cobra.Reaction, pydot.Node], Dict[cobra.Reaction, pydot.Node]
    ]:
        """Draw the solution using graphviz.

        :return: (g_dot, r_nodes, m_nodes) - g_dot is the Dot object for
        drawing the pathway, r_nodes and m_nodes are dictionaries containing
        all the nodes in the bipartite graph (for easy access later)
        """
        g_dot = pydot.Dot(newrank=True)

        r_nodes: Dict[cobra.Reaction, pydot.Node] = {}  # dict of reaction nodes
        m_nodes: Dict[cobra.Reaction, pydot.Node] = {}  # dict of metabolite

        for row in self.flux_df.itertuples():
            node = Solution.create_reaction_node(row.reaction)
            r_nodes[row.reaction_id] = node
            g_dot.add_node(node)

            for met in row.reaction.metabolites:
                if met in self.cofactor_set:
                    # make a special node of this co-factor for each reaction
                    # that uses it
                    m_node = self.create_metabolite_node(met)
                    g_dot.add_node(m_node)
                elif met not in m_nodes:
                    # This is a normal metabolite, create only one node
                    # for it
                    m_node = self.create_metabolite_node(met)
                    g_dot.add_node(m_node)
                    m_nodes[met] = m_node
                else:
                    m_node = m_nodes[met]

                edge = Solution.create_edge(
                    m_node,
                    r_nodes[row.reaction_id],
                    weight=1.0 if met in self.cofactor_set else 5.0,
                    flux=row.reaction.get_coefficient(met) * row.primal,
                )
                g_dot.add_edge(edge)
        return g_dot, r_nodes, m_nodes

    def to_gjgf(self) -> dict:
        """Export the solution in gJGF format."""

        # first create all the nodes and edges, then add them to the
        # dictionary

        cofactor_ids = [met.id for met in self.cofactor_set]

        nodes = {}
        edges = []
        for row in self.flux_df.itertuples():
            rxn = row.reaction
            nodes[rxn.id] = {
                "label": rxn.id,
                "metadata": {
                    "size": 20,
                    "hover": "Reaction: " + rxn.id,
                    "shape": "rectangle",
                    "color": "green",
                    "label_color": "darkgreen",
                    "label_size": 12,
                },
            }

            for met in rxn.metabolites:
                if met.id in cofactor_ids:
                    # make a special node of this co-factor for each reaction
                    # that uses it
                    nodes[met.id + "@" + rxn.id] = {
                        "label": met.id[0:-2],
                        "metadata": {
                            "size": 4,
                            "hover": "Cofactor: " + met.name,
                            "shape": "circle",
                            "color": "cyan",
                            "border_color": "white",
                            "label_color": "cyan",
                            "label_size": 10,
                        },
                    }
                elif met.id not in nodes:
                    # create the node only if it hasn't already been
                    # created for another reaction
                    nodes[met.id] = {
                        "label": met.id[0:-2],
                        "metadata": {
                            "size": 10,
                            "label_size": 12,
                            "shape": "circle",
                            "color": "blue",
                            "label_color": "darkblue",
                            "hover": "Metabolite: " + met.name,
                        },
                    }
                    if met.id[0:2] == "__":  # highlight this as source/sink
                        nodes[met.id]["metadata"]["shape"] = "hexagon"
                        nodes[met.id]["metadata"]["color"] = "magenta"

                flux = np.round(row.reaction.get_coefficient(met) * row.primal, 2)
                source = rxn.id
                if met.id in cofactor_ids:
                    target = met.id + "@" + rxn.id
                    edge_color = "cyan"
                else:
                    target = met.id
                    edge_color = "blue"
                if flux < 0:
                    source, target = target, source
                edges.append(
                    {
                        "source": source,
                        "target": target,
                        "label": f"{abs(flux):g}",
                        "metadata": {
                            "color": edge_color,
                            "label_size": 8,
                            "size": abs(flux)
                        },
                    }
                )

        return {
            'graph': {
                "label": self.name,
                "directed": True,
                "metadata": {
                    "background_color": "white",
                },
                "nodes": nodes,
                "edges": edges,
            },
        }

    def gravis(self, backend="d3", **kwargs) -> gravis._internal.plotting.data_structures.Figure:
        if backend == "d3":
            return gravis.d3(self.to_gjgf(), **kwargs)
        elif backend == "vis":
            return gravis.vis(self.to_gjgf(), **kwargs)
        elif backend == "three":
            return gravis.three(self.to_gjgf(), **kwargs)
        else:
            raise ValueError("Unknown Gravis backend: " + backend)
