# The MIT License (MIT)

# Copyright (c) 2023 Weizmann Institute of Science
# Copyright (c) 2023 Elad Noor

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import warnings
from io import StringIO
from typing import List, Tuple

import numpy as np
import pandas as pd
import requests
from Bio import SeqIO, SeqUtils
from cobra.core import Model
from enkie import (CompartmentParameters, Enzyme, Metabolite, ModularRateLaw,
                   ParameterSpace, Reaction)
from enkie.io.cobra import get_parameters_for_reaction_subset
from equilibrator_api import ComponentContribution
from equilibrator_api.model import Bounds
from sbtab import SBtab

from util import cobra_met_to_equilibrator

DEFAULT_COMPOUND_MOLECULAR_MASS = 1000.0
DEFAULT_PROTEIN_MOLECULAR_MASS = 10000.0
DEFAULT_KCAT_GMEAN = 1.0
DEFAULT_KCAT_FW = 1.0
DEFAULT_KCAT_BW = 1.0
DEFAULT_KM = 1.0


# ensure we have the needed R packages
try:
    from rpy2.robjects.packages import PackageNotInstalledError, importr

    importr("base")
    importr("brms")
except ImportError:
    raise ImportError(
        "R together with the `brms` package must be installed to run ENKIE"
    )
except PackageNotInstalledError:
    raise ImportError(
        "R together with the `brms` package must be installed to run ENKIE"
    )


def get_protein_molecular_mass(enzyme: Enzyme) -> float:
    if len(enzyme.uniprot_acs) > 0:
        uniprot_id = enzyme.uniprot_acs[0]
    else:
        warnings.warn(
            f"The enzyme {enzyme} has no UniProt ID."
            "Using default molecular mass of 10 kDa."
        )
        return 10000.0
    url = f"http://www.uniprot.org/uniprot/{uniprot_id}.fasta"
    response = requests.post(url)
    pSeq = list(SeqIO.parse(StringIO("".join(response.text)), "fasta"))
    if len(pSeq) == 0:
        warnings.warn(
            f"Failed to find the UniProt sequence for the enzyme {enzyme}."
            "Using default molecular mass of 10 kDa."
        )
        return 10000.0

    return SeqUtils.molecular_weight(pSeq[0], seq_type="protein")


def get_parameters_from_enkie(
    flux_sbtab: SBtab.SBtabTable, cobra_model: Model
) -> Tuple[
    List[Reaction], List[ModularRateLaw], List[Enzyme], List[Metabolite], ParameterSpace
]:
    flux_df = flux_sbtab.to_data_frame()
    compartment_parameters = CompartmentParameters.load("e_coli")
    reactions, rate_laws, enzymes, metabolites = get_parameters_for_reaction_subset(
        cobra_model, flux_df.Reaction.to_list()
    )
    parameter_space = ParameterSpace(
        reactions, rate_laws, enzymes, metabolites, compartment_parameters
    )
    return reactions, rate_laws, enzymes, metabolites, parameter_space


def add_ecm_params_to_sbtab(
    sbtabdoc: SBtab.SBtabDocument,
    cobra_model: Model,
    comp_contrib: ComponentContribution = None,
    bounds: Bounds = None,
    write_thermodynamics: bool = False,
) -> Tuple[pd.DataFrame, SBtab.SBtabDocument]:
    if comp_contrib is None:
        comp_contrib = ComponentContribution()

    reaction_sbtab = sbtabdoc.get_sbtab_by_id("Reaction")
    flux_sbtab = sbtabdoc.get_sbtab_by_id("Flux")

    (
        reactions,
        rate_laws,
        enzymes,
        metabolites,
        parameter_space,
    ) = get_parameters_from_enkie(flux_sbtab, cobra_model)

    # Create Configuration table
    config_df = pd.DataFrame(
        data=[
            ("algorithm", "ECM"),
            ("version", "3"),
            ("kcat_source", "gmean"),
            ("denominator", "CM"),
            ("regularization", "volume"),
            ("p_h", str(comp_contrib.p_h)),
            ("ionic_strength", str(comp_contrib.ionic_strength)),
            ("p_mg", str(comp_contrib.p_mg)),
            ("dg_confidence", "0.95"),
        ],
        columns=["!Option", "!Value"],
    )
    config_sbtab = SBtab.SBtabTable.from_data_frame(
        config_df, table_id="Configuration", table_type="Config"
    )

    # Create Compound table
    met2eq = {}
    compound_data = []
    for metabolite in metabolites:
        met_id, compound_obj = cobra_met_to_equilibrator(
            metabolite.id, comp_contrib=comp_contrib
        )
        if not comp_contrib.ccache.is_proton(compound_obj):
            met2eq[metabolite.id] = compound_obj
            compound_data.append((metabolite.id, met_id))

    compound_df = pd.DataFrame(data=compound_data, columns=["!ID", "!Identifiers"])
    compound_sbtab = SBtab.SBtabTable.from_data_frame(
        compound_df, table_id="Compound", table_type="Compound"
    )

    # Create EnzymeCostWeight table
    enzyme_weight_df = pd.DataFrame(
        data=[r.id for r in reactions], columns=["!Reaction"]
    )
    enzyme_weight_df.insert(0, "!QuantityType", "enzyme cost weight")
    enzyme_weight_df.insert(2, "!Value", "1")
    enzyme_weight_sbtab = SBtab.SBtabTable.from_data_frame(
        enzyme_weight_df, table_id="EnzymeCostWeight", table_type="Quantity"
    )
    enzyme_weight_sbtab.change_attribute("Unit", "arbitrary unit")

    # Create Parameter table
    param_mean = parameter_space.mean
    kcat_gmean = np.exp(param_mean.loc["ln_kv"])
    kcat_fwd = np.exp(param_mean.loc["ln_kcat_fw"])
    kcat_bwd = np.exp(param_mean.loc["ln_kcat_bw"])
    drg0 = param_mean.loc["drg0"]

    # ln_km are natural-log of the Km in molar, so we convert that to mM using: y = 1000 * e^x
    km = 1000 * np.exp(param_mean.loc["ln_km"])

    # if the compound object has no mass (usually because this is a compound with an
    # unspecific side group) we use a default of 1000 Da
    parameter_data = [
        (
            "molecular mass",
            "",
            m_id,
            str(compound_obj.mass or DEFAULT_COMPOUND_MOLECULAR_MASS),
            "Da",
        )
        for m_id, compound_obj in met2eq.items()
    ]
    added_reaction_ids = set()
    for enzyme, rate_law in zip(enzymes, rate_laws):
        reaction = rate_law.reaction
        if reaction.id in added_reaction_ids:
            continue
        added_reaction_ids.add(reaction.id)
        molecular_mass = get_protein_molecular_mass(enzyme)
        parameter_data += [
            (
                "catalytic rate constant geometric mean",
                reaction.id,
                "",
                str(kcat_gmean.get(reaction.id, DEFAULT_KCAT_GMEAN)),
                "1/s",
            ),
            (
                "substrate catalytic rate constant",
                reaction.id,
                "",
                str(kcat_fwd.get(reaction.id, DEFAULT_KCAT_FW)),
                "1/s",
            ),
            (
                "product catalytic rate constant",
                reaction.id,
                "",
                str(kcat_bwd.get(reaction.id, DEFAULT_KCAT_BW)),
                "1/s",
            ),
            ("protein molecular mass", reaction.id, "", str(molecular_mass), "Da"),
        ]
        for metabolite_id in reaction.metabolite_ids:
            idx = f"{metabolite_id}_{reaction.id}"

            # use 1 mM as the default Km value if it is missing
            parameter_data.append(
                (
                    "Michaelis constant",
                    reaction.id,
                    metabolite_id,
                    str(km.get(idx, DEFAULT_KM)),
                    "mM",
                )
            )

    # There might be some spontaneous reactions left without kinetic data. For example,
    # MOX (malate oxidase: mal__L_c + o2_c ⇌ h2o2_c + oaa_c) is not associated with
    # any genes and therefore will not apprear in the enzyme list. Therefore,
    # we add "fake" default kinetic parameters just so that we can later run ECM.
    for reaction in reactions:
        if reaction.id in added_reaction_ids:
            continue
        parameter_data += [
            (
                "catalytic rate constant geometric mean",
                reaction.id,
                "",
                str(DEFAULT_KCAT_GMEAN),
                "1/s",
            ),
            (
                "substrate catalytic rate constant",
                reaction.id,
                "",
                str(DEFAULT_KCAT_FW),
                "1/s",
            ),
            (
                "product catalytic rate constant",
                reaction.id,
                "",
                str(DEFAULT_KCAT_BW),
                "1/s",
            ),
            (
                "protein molecular mass",
                reaction.id,
                "",
                str(DEFAULT_PROTEIN_MOLECULAR_MASS),
                "Da",
            ),
        ]

    param_df = pd.DataFrame(
        data=parameter_data,
        columns=["!QuantityType", "!Reaction", "!Compound", "!Value", "!Unit"],
    )
    param_sbtab = SBtab.SBtabTable.from_data_frame(
        param_df, table_id="Parameter", table_type="Quantity"
    )

    # Create ConcentrationConstraint table
    bounds = bounds or Bounds.get_default_bounds(comp_contrib)

    concentration_df = pd.DataFrame(
        data=[
            (
                "concentration",
                m_id,
                str(bounds.get_lower_bound(compound_obj).m_as("mM")),
                str(bounds.get_upper_bound(compound_obj).m_as("mM")),
            )
            for m_id, compound_obj in met2eq.items()
        ],
        columns=["!QuantityType", "!Compound", "!Min", "!Max"],
    )
    concentration_sbtab = SBtab.SBtabTable.from_data_frame(
        concentration_df, table_id="ConcentrationConstraint", table_type="Quantity"
    )
    concentration_sbtab.change_attribute("Unit", "mM")

    if write_thermodynamics:
        # Create Thermodynamics table
        thermo_df = pd.DataFrame(
            data=[
                (
                    "reaction gibbs energy",
                    reaction.id,
                    "",
                    str(drg0[reaction.id]),
                    "kJ/mol",
                )
                for reaction in reactions
            ],
            columns=["!QuantityType", "!Reaction", "!Compound", "!Value", "!Unit"],
        )
        thermo_sbtab = SBtab.SBtabTable.from_data_frame(
            thermo_df, table_id="Thermodynamics", table_type="Quantity"
        )
        thermo_sbtab.change_attribute("StandardConcentration", "M")
    else:
        thermo_sbtab = None

    # create the new SBtab with all relevant tables for running ECM
    new_sbtabdoc = SBtab.SBtabDocument(name="ECM", filename="ecm_config.tsv")
    new_sbtabdoc.add_sbtab(config_sbtab)
    new_sbtabdoc.add_sbtab(reaction_sbtab)
    new_sbtabdoc.add_sbtab(compound_sbtab)
    if write_thermodynamics:
        new_sbtabdoc.add_sbtab(thermo_sbtab)
    new_sbtabdoc.add_sbtab(flux_sbtab)
    new_sbtabdoc.add_sbtab(param_sbtab)
    new_sbtabdoc.add_sbtab(concentration_sbtab)
    new_sbtabdoc.add_sbtab(enzyme_weight_sbtab)
    return parameter_space, new_sbtabdoc
